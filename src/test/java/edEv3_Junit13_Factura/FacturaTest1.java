package edEv3_Junit13_Factura;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class FacturaTest1 {
	Factura factura;
	Producto p1;
	Producto p2;
	Producto p3;
	@BeforeEach
	void setUp() throws Exception {
		factura = new Factura();
		p1 =  new Producto("mandarina",12,0.09f);
		p2 =  new Producto("fresas",20,0.05f);
		p3 =  new Producto("tomates",100,0.10f);
	}

	@Test
	void test() {
		assertEquals(p1.getNombre(),"mandarina");
		assertEquals(p1.getPrecio(),0.09f);
		assertEquals(p1.getCantidad(),12);
		
		//Test total
		assertEquals(p1.precioTotal(),(12*0.09f));
	}
	
	void FacturaTest() {
		factura.meterProducto(p1);
		factura.meterProducto(p2);
		factura.meterProducto(p3);
		
		float totalFacturaEsperado = (12*0.09f)+(20*0.05f)+(100*0.10f);
		float totalFacturaEsperadoConIva = ((12*0.09f)+0.3f)+((20*0.05f)+0.3f)+((100*0.10f)+0.3f);
		assertEquals(factura.totalFactura(),totalFacturaEsperado);
		assertEquals(factura.aplicarIva(0.3f),totalFacturaEsperadoConIva);
	}

}
