package edEv3_Junit13_Factura;

import java.util.ArrayList;

/***
 * 
 * @author liher
 *
 */
public class Factura {

	private ArrayList<Producto>productos;

	public Factura(){
		productos = new ArrayList();
	}
	
	public void meterProducto(Producto p) {
		productos.add(p);
	}
	
	public float totalFactura() {
		float total=0;
		
		for(Producto p:productos) {
			total += p.precioTotal();
		}
		
		return total;
	}
	
	public float aplicarIva(float iva) {
		float total=0;
		for(Producto p:productos) {
			total += p.precioTotal()+iva;
		}		
		return total;
	}

	public ArrayList<Producto> getProductos() {
		return productos;
	}

	public void setProductos(ArrayList<Producto> productos) {
		this.productos = productos;
	}
	
	
	
}
